import { combineReducers } from 'redux'
import detailId from './detailId'
import dataFilm from './dataFilm'

export const rootReducer = combineReducers({
  detailId,
  dataFilm
})
