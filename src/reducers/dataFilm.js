import cleanSet from 'clean-set'

export const DATA_FILM = 'DATA_FILM'

const initialState = {
  data: {}
}

export default function DetailId (state = initialState, action) {
  switch (action.type) {
    case DATA_FILM:
      return cleanSet(state, 'data', action.payload)
    default:
      return state
  }
}
