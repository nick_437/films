// package
import Box from '@material-ui/core/Box'
import cn from 'classnames'
import _ from 'lodash'
// hooks
import useFilmDetail from './useFilmDetail'
// style
import css from './ContainerFilmDetail.module.scss'

export default function ContainerFilmDetail({ className }) {
  const {
    isLoading,
    isError,
    error,
    dataFilm,
    title,
    opening_crawl: openingCrawl
  } = useFilmDetail()

  if (isLoading) {
    return <Box className={css.loading}>Loading...</Box>
  }

  if (isError) {
    return <Box className={css.error}>Error: {error?.message}</Box>
  }

  if (_.isEmpty(dataFilm)) {
    return <Box className={css.empty}>Please select a movie</Box>
  }

  return (
    <Box className={cn(css.wrapper, className)}>
      {title && (
        <h1>{title}</h1>
      )}
      {openingCrawl && (
        <p>{openingCrawl}</p>
      )}
    </Box>
  )
}
