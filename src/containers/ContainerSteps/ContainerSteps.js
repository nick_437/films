// package
import Box from '@material-ui/core/Box'
import _ from 'lodash'
import cn from 'classnames'
// components
import Step from '../../components/Step/Step'
// json
import steps from '../../../public/server/steps.json'
// style
import css from './ContainerSteps.module.scss'

export default function ContainerSteps({ active }) {
  return (
    <Box className={css.container}>
      {_.map(steps, (item, key) => (
        <Step
          {...item}
          key={key + 1}
          className={cn(css.step, {
            [css._active]: active === key + 1
          })}
        />
      ))}
    </Box>
  )
}
