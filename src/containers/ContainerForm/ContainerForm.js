// package
import React from 'react'
import { Form, Field } from 'react-final-form'
import Box from '@material-ui/core/Box'
import _ from 'lodash'
// components
import Button from '../../components/Button/Button'
// hooks
import useForm from './useForm'
// style
import css from './ContainerForm.module.scss'

export default function ContainerForm({
  className,
  setCurrentStep,
  currentStep
}) {
  const { onSubmit, email } = useForm({ setCurrentStep, currentStep })

  return (
    <Form
      onSubmit={onSubmit}
      validate={values => {
        const errors = {}
        if (!values.username) {
          errors.username = 'required'
        }
        if (!values.email) {
          errors.email = 'required'
        } else if (email(values.email)) {
          errors.email = 'invalid email'
        }
        if (!values.review) {
          errors.review = 'required'
        }
        return errors
      }}
      render={({
        handleSubmit,
        form,
        submitting,
        pristine,
        values,
        errors
      }) => {
        return (
          <form
            className={css.form}
            onSubmit={handleSubmit}
          >
            <Field name="username">
              {({ input, meta }) => (
                <Box className={css.row}>
                  <label>username</label>
                  <input {...input} type='text' placeholder='username' />
                  {meta.error && meta.touched && <span className={css.error}>{meta.error}</span>}
                </Box>
              )}
            </Field>
            <Field name="email">
              {({ input, meta }) => (
                <Box className={css.row}>
                  <label>email</label>
                  <input {...input} type='text' placeholder='email' />
                  {meta.error && meta.touched && <span className={css.error}>{meta.error}</span>}
                </Box>
              )}
            </Field>
            <Field name="review">
              {({ input, meta }) => (
                <Box className={css.row}>
                  <label>Username</label>
                  <textarea {...input} placeholder='review' />
                  {meta.error && meta.touched && <span className={css.error}>{meta.error}</span>}
                </Box>
              )}
            </Field>
            <Box className={css.buttons}>
              <Button
                type='submit'
                disabled={submitting || pristine || !_.isEmpty(errors)}
                variant='contained'
                color='primary'
                className={css.button}
              >
                Submit
              </Button>
              <Button
                type='button'
                fnClick={form.reset}
                disabled={submitting || pristine}
                variant='contained'
                color='primary'
                className={css.button}
              >
                Reset
              </Button>
            </Box>
          </form>
        )
      }}
    />
  )
}