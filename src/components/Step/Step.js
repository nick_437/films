// package
import Box from '@material-ui/core/Box'
import cn from 'classnames'
// style
import css from './Step.module.scss'

export default function StepComponent({ text, className }) {
  return (
    <Box className={cn(css.container, className)}>
      {text && text}
    </Box>
  )
}
