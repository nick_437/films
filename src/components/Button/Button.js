// package
import Button from '@material-ui/core/Button'
// style
// import css from './Button.module.scss'

export default function ButtonComponent({ className, children, fnClick, ...extraProps }) {
  return (
    <Button
      {...extraProps}
      className={className}
      onClick={fnClick}
    >
      {children && children}
    </Button>
  )
}
