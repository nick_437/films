// package
import Box from '@material-ui/core/Box'
import cn from 'classnames'
// style
import css from './Film.module.scss'

export default function ComponentsFilm({ title, id, className, fnClickFilm }) {
  return (
    <Box
      className={cn(css.container, className)}
      onClick={() => fnClickFilm(id)}
    >
      {title && title}
    </Box>
  )
}
