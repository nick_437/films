// packages
import { useCallback, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
// constants
import { DATA_FILM } from '../../reducers/dataFilm'
import { DETAIL_ID } from '../../reducers/detailId'

export default function useHoom() {
  const dispatch = useDispatch()
  const id = useSelector(state => state.detailId.detailId)
  const dataFilm = useSelector(state => state.dataFilm.data)
  // initial step
  const [currentStep, setCurrentStep] = useState(1)

  const handleClickReturnStart = useCallback(
    () => {
      setCurrentStep(1)
      dispatch({
        type: DATA_FILM,
        payload: {}
      })
      dispatch({
        type: DETAIL_ID,
        payload: null
      })
    }, [dispatch, setCurrentStep]
  )

  return {
    dataFilm,
    id,
    currentStep,
    setCurrentStep,
    handleClickReturnStart
  }
}
