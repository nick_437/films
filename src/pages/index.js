// package
import Container from '@material-ui/core/Container'
import Box from '@material-ui/core/Box'
// components
import Button from '../components/Button/Button'
// containers
import Steps from '../containers/ContainerSteps/ContainerSteps'
import Films from '../containers/ContainerFilms/ContainerFilms'
import FilmsDetail from '../containers/ContainerFilmDetail/ContainerFilmDetail'
import ContainerForm from '../containers/ContainerForm/ContainerForm'
// hooks
import useHoom from './hooks/useHoom'
// style
import css from '../../styles/Home.module.scss'

export default function Home() {
  const {
    currentStep,
    setCurrentStep,
    id,
    handleClickReturnStart,
    dataFilm
  } = useHoom()
  return (
    <Container maxWidth='md' className={css.wrapper}>
      <Steps active={currentStep} />
      {currentStep === 1 && (
        <Box className={css.container}>
          <Films className={css.films} />
          <FilmsDetail className={css.films_detail} />
        </Box>
      )}
      {currentStep === 2 && (
        <Box className={css.container}>
          <ContainerForm
            setCurrentStep={setCurrentStep}
            currentStep={currentStep}
          />
        </Box>
      )}
      {currentStep === 3 && (
        <Box className={css.container}>
          {`Thanks for the review of the movie ${dataFilm?.title}`}
        </Box>
      )}
      {id && (
        <Box>
          {currentStep > 1 && currentStep < 3 && (
            <Button
              className={css.btn}
              color='primary'
              variant='contained'
              fnClick={() => setCurrentStep(currentStep - 1)}
            >
              Prev step
            </Button>
          )}
          {currentStep === 1 && (
            <Button
              className={css.btn}
              color='primary'
              variant='contained'
              fnClick={() => setCurrentStep(currentStep + 1)}
            >
              Next step
            </Button>
          )}
          {currentStep === 3 && (
            <Button
              className={css.btn}
              color='primary'
              variant='contained'
              fnClick={handleClickReturnStart}
            >
              Return to start
            </Button>
          )}
        </Box>
      )}
    </Container>
  )
}
